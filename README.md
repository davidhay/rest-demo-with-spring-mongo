# Rest Demo with Spring Boot and Spring Data Mongo #

This Java Application also creates a Rest API for Users/Tasks. This app uses SpringBoot/SpringDataMongo.

The rest endpoints have integration tests written using Spring's MockMVC and embedded MongoDB. (It's possible to use Spring's MockMVC together with Cucumber for BDD acceptance testing).

## REST URLS FOR READ/GET ##

* http://localhost:8080/users
* http://localhost:8080/users/{userid}
* http://localhost:8080/users/{userid}/tasks
* http://localhost:8080/users/{userid}/tasks/{taskid}

## REST URLS POST/CREATE ##

to create a new user 

post to http://localhost:8080/users

```
#!python

curl -i -X POST -H "Content-Type: application/json" -d '{"firstname" : "santa","lastname" : "Saint", "username" : "Nicholas"}' http://localhost:8080/users

HTTP/1.1 201 Created
Server: Apache-Coyote/1.1
Location: http://localhost:8080/users/6
Content-Length: 0
Date: Sun, 12 Apr 2015 21:28:54 GMT
```



to create a new user task

post to http://localhost:8080/users/1/tasks


```
#!python

curl -i -X POST -H "Content-Type: application/json" -d \
'{"description":"desc","completed":false,"dueDate":"2015-12-31T23:00:00.000+0000","priority":"HIGH"}' \
 http://localhost:8080/users/1/tasks

HTTP/1.1 201 Created
Server: Apache-Coyote/1.1
Location: http://localhost:8080/users/1/tasks/10
Content-Length: 0
Date: Sun, 12 Apr 2015 21:28:21 GMT
```


To run the java app


```
#!python

$ mvn spring-boot:run
```