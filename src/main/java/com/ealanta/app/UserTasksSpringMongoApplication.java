package com.ealanta.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserTasksSpringMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserTasksSpringMongoApplication.class, args);
    }
}
