package com.ealanta.app.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ealanta.app.domain.User;

public interface UserRepository extends MongoRepository<User, Long>{

}
