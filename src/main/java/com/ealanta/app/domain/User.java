package com.ealanta.app.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

@Document(collection = "users")
public class User {

	@JsonView(View.Summary.class)
	@Id
	private long id;

	@JsonView(View.Summary.class)
	private String username;

	@JsonView(View.Summary.class)
	@JsonProperty(value="firstname")
	private String first;

	@JsonView(View.Summary.class)
	@JsonProperty(value="lastname")
	private String last;
		
	private Date now = new Date();

	private final List<Task> tasks = new ArrayList<Task>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	@Override
	public String toString() {
		return String.format("username[%s] first[%s] last[%s]", username,
				first, last);
	}

	public void setTasks(List<Task> tasks) {
		if (tasks == null) {
			return;
		}
		this.tasks.clear();
		for (Task t : tasks) {
			addTask(t);
		}
	}

	public void addTask(Task task) {
		if (task.getTaskId() < 1) {
			Long maxId = this.tasks.stream().map(t -> t.getTaskId())
					.max(Long::compare).orElse(0L);
			task.setTaskId(maxId + 1);
			this.tasks.add(task);
		}
	}

	public List<Task> getTasks() {
		return tasks;
	}
	
	public Date getNow(){
		return now;
	}

}
