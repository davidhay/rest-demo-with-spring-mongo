package com.ealanta.app.domain;

public enum Priority {
	LO,
	MED,
	HIGH
}
