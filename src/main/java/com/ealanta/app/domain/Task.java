package com.ealanta.app.domain;

import java.util.Date;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;


public class Task {

	@Indexed
	@Field(value="id")
	private long taskId;
	
	private String description;
	private Date dueDate;
	private Priority priority;
	private boolean completed;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Priority getPriority() {
		return priority;
	}
	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
	
	public long getTaskId() {
		return taskId;
	}
	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}
	
	@Override
	public String toString() {
		return String.format("id[%d] description[%s] dueDate[%s] completed[%s] priority[%s]", taskId, description, dueDate, completed, priority);
	}
	
}
