package com.ealanta.app.controller;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ealanta.app.domain.Task;
import com.ealanta.app.domain.User;
import com.ealanta.app.domain.View;
import com.ealanta.app.repo.UserRepository;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
@RequestMapping("users")
public class UserTasksController {

	@Autowired
	private UserRepository userRepo;

	@RequestMapping(method = RequestMethod.GET)
	@JsonView(View.Summary.class)
	public List<User> getUsers() {
		return userRepo.findAll();
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public ResponseEntity<User> getUser(@PathVariable("userId") Long userId) {
		User user = userRepo.findOne(userId);
		if (user == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/{userId}/tasks", method = RequestMethod.GET)
	public ResponseEntity<List<Task>> getUserTasks(
			@PathVariable("userId") Long userId) {
		User user = userRepo.findOne(userId);
		if (user == null) {
			return new ResponseEntity<List<Task>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Task>>(user.getTasks(),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/{userId}/tasks/{taskId}", method = RequestMethod.GET)
	public ResponseEntity<Task> getUserTask(
			@PathVariable("userId") Long userId,
			@PathVariable("taskId") Long taskId) {
		User user = userRepo.findOne(userId);
		List<Task> tasks = user.getTasks();
		Optional<Task> taskO = tasks.stream()
				.filter(t -> t.getTaskId() == taskId).findFirst();
		if (taskO.isPresent()) {
			Task newTask = taskO.get();
			return new ResponseEntity<Task>(newTask, HttpStatus.OK);
		} else {
			return new ResponseEntity<Task>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/{userId}/tasks", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Task> createUserTask(
			@PathVariable("userId") Long userId, @RequestBody Task task) {
		Assert.notNull(userId);
		User user = userRepo.findOne(userId);
		if (user == null) {
			return new ResponseEntity<Task>(HttpStatus.NOT_FOUND);
		}
		user.addTask(task);
		userRepo.save(user);
		HttpHeaders headers = new HttpHeaders();
		Map<String, String> params = new HashMap<>();
		params.put("userId", String.valueOf(userId));
		params.put("taskId", String.valueOf(task.getTaskId()));
		URI uriOfUser = ServletUriComponentsBuilder.fromCurrentContextPath()
				.pathSegment("users/{userId}/tasks/{taskId}")
				.buildAndExpand(params).toUri();
		headers.setLocation(uriOfUser);
		return new ResponseEntity<Task>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> createUser(@RequestBody User user) throws IOException {
		user.setId(userRepo.count() + 1);
		User saved = userRepo.save(user);
		HttpHeaders headers = new HttpHeaders();
		Map<String, String> params = new HashMap<>();
		params.put("userId", String.valueOf(saved.getId()));
		URI uriOfUser = ServletUriComponentsBuilder.fromCurrentContextPath()
				      .pathSegment("users/{userId}").buildAndExpand(params).toUri();
		headers.setLocation(uriOfUser);
		return new ResponseEntity<User>(headers, HttpStatus.CREATED);
	}

}
