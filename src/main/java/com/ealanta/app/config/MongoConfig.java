package com.ealanta.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
public class MongoConfig extends AbstractMongoConfiguration
{

    @Override
    protected String getDatabaseName() {
        return "usertasks";
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient("localhost:27017");
    }


}
