package com.ealanta.app;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.ealanta.app.domain.Priority;
import com.ealanta.app.domain.Task;
import com.ealanta.app.domain.User;
import com.ealanta.app.repo.UserRepository;
import com.mongodb.Mongo;

import cz.jirutka.spring.embedmongo.EmbeddedMongoBuilder;

/**
 * @author davidhay
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserTasksSpringMongoApplication.class)
@WebAppConfiguration
public class RestUserTest {

	private SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Configuration
	static class Config extends AbstractMongoConfiguration {
		@Bean(destroyMethod = "close")
		public Mongo mongo() throws IOException {
			return new EmbeddedMongoBuilder().version("2.4.5")
					.bindIp("127.0.0.1").port(55555).build();
		}

		@Override
		protected String getDatabaseName() {
			return "usertasks";
		}
	}

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Autowired
	private UserRepository userRepo;

	private String randUsername;

	private String randFirst;

	private String randLast;

	private Date getDate(String value) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(SDF.parse(value));
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		userRepo.deleteAll();
		// USER 1
		User user1 = new User();
		user1.setId(1L);
		user1.setUsername("joebloggs");
		user1.setFirst("Joe");
		user1.setLast("Bloggs");

		Task task11 = new Task();
		task11.setCompleted(false);
		task11.setDescription("task eleven");
		task11.setDueDate(getDate("2016-01-01 01:02:03"));
		task11.setPriority(Priority.LO);

		// 13
		Task task12 = new Task();
		task12.setCompleted(true);
		task12.setDescription("task twelve");
		task12.setDueDate(getDate("2016-01-02 02:03:04"));
		task12.setPriority(Priority.MED);

		Task task13 = new Task();
		task13.setCompleted(false);
		task13.setDescription("task thirteen");
		task13.setDueDate(getDate("2016-01-03 03:04:05"));
		task13.setPriority(Priority.HIGH);
		user1.setTasks(Arrays.asList(task11, task12, task13));
		
		
		// USER 2
		User user2 = new User();

		user2.setId(2L);
		user2.setUsername("molliem");
		user2.setFirst("Mollie");
		user2.setLast("Malone");

		Task task21 = new Task();
		task21.setCompleted(false);
		task21.setDescription("task twenty one");
		task21.setDueDate(getDate("2016-01-04 04:05:06"));
		task21.setPriority(Priority.HIGH);
		  
		user2.setTasks(Arrays.asList(task21));
		

		// USER 3
		User user3 = new User();
		user3.setId(3L);
		randUsername = UUID.randomUUID().toString();
		randFirst = UUID.randomUUID().toString();
		randLast = UUID.randomUUID().toString();
		user3.setUsername(randUsername);
		user3.setFirst(randFirst);
		user3.setLast(randLast);
		userRepo.insert(user1);
		userRepo.insert(user2);
		userRepo.insert(user3);
	}

	@Test
	public void testGetUsers() throws Exception {
		mockMvc.perform(get("/users"))
				.andDo(print())
				.andExpect(
						content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$", hasSize(3)))

				.andExpect(jsonPath("$[0].firstname", is(equalTo("Joe"))))
				.andExpect(jsonPath("$[0].lastname", is(equalTo("Bloggs"))))
				.andExpect(jsonPath("$[0].username", is(equalTo("joebloggs"))))

				.andExpect(jsonPath("$[1].firstname", is(equalTo("Mollie"))))
				.andExpect(jsonPath("$[1].lastname", is(equalTo("Malone"))))
				.andExpect(jsonPath("$[1].username", is(equalTo("molliem"))))

				.andExpect(jsonPath("$[2].firstname", is(equalTo(randFirst))))
				.andExpect(jsonPath("$[2].lastname", is(equalTo(randLast))))
				.andExpect(jsonPath("$[2].username", is(equalTo(randUsername))))

				.andExpect(status().isOk());
	}

	@Test
	public void testGetUser1() throws Exception {
		mockMvc.perform(get("/users/1"))
				.andDo(print())
				.andExpect(
						content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.firstname", is(equalTo("Joe"))))
				.andExpect(jsonPath("$.lastname", is(equalTo("Bloggs"))))
				.andExpect(jsonPath("$.username", is(equalTo("joebloggs"))))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetUser2() throws Exception {
		mockMvc.perform(get("/users/2"))
				.andDo(print())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.firstname", is(equalTo("Mollie"))))
				.andExpect(jsonPath("$.lastname", is(equalTo("Malone"))))
				.andExpect(jsonPath("$.username", is(equalTo("molliem"))))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetUser1Tasks() throws Exception {
		mockMvc.perform(get("/users/1/tasks"))
				.andDo(print())
				// 11
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$[0].description", is(equalTo("task eleven"))))
				.andExpect(jsonPath("$[0].completed", is(equalTo(false))))
				.andExpect(jsonPath("$[0].dueDate",is(equalTo("2016-01-01T01:02:03.000+0000"))))
				.andExpect(jsonPath("$[0].priority", is(equalTo("LO"))))
				// 12
				.andExpect(jsonPath("$[1].description", is(equalTo("task twelve"))))
				.andExpect(jsonPath("$[1].completed", is(equalTo(true))))
				.andExpect(jsonPath("$[1].dueDate",is(equalTo("2016-01-02T02:03:04.000+0000"))))
				.andExpect(jsonPath("$[1].priority", is(equalTo("MED"))))
				// 13
				.andExpect(jsonPath("$[2].description",is(equalTo("task thirteen"))))
				.andExpect(jsonPath("$[2].completed", is(equalTo(false))))
				.andExpect(jsonPath("$[2].dueDate",is(equalTo("2016-01-03T03:04:05.000+0000"))))
				.andExpect(jsonPath("$[2].priority", is(equalTo("HIGH"))))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetUser1Task1() throws Exception {
		mockMvc.perform(get("/users/1/tasks/1"))
				.andDo(print())
				// 11
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.description", is(equalTo("task eleven"))))
				.andExpect(jsonPath("$.completed", is(equalTo(false))))
				.andExpect(jsonPath("$.dueDate",is(equalTo("2016-01-01T01:02:03.000+0000"))))
				.andExpect(jsonPath("$.priority", is(equalTo("LO"))))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetUser1Task2() throws Exception {
		mockMvc.perform(get("/users/1/tasks/2"))
				.andDo(print())
				// 12
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.description", is(equalTo("task twelve"))))
				.andExpect(jsonPath("$.completed", is(equalTo(true))))
				.andExpect(jsonPath("$.dueDate",is(equalTo("2016-01-02T02:03:04.000+0000"))))
				.andExpect(jsonPath("$.priority", is(equalTo("MED"))))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetUser1Task3() throws Exception {
		mockMvc.perform(get("/users/1/tasks/3"))
				.andDo(print())
				// 13
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.description", is(equalTo("task thirteen"))))
				.andExpect(jsonPath("$.completed", is(equalTo(false))))
				.andExpect(jsonPath("$.dueDate",is(equalTo("2016-01-03T03:04:05.000+0000"))))
				.andExpect(jsonPath("$.priority", is(equalTo("HIGH"))))
				.andExpect(status().isOk());
	}

	@Test
	public void testCreateUser() throws Exception {
		String userJson = "{\"firstname\" : \"Saint\",\"lastname\" : \"Nicholas\", \"username\" : \"santa\"}";
		mockMvc.perform(
				post("/users/").contentType(MediaType.APPLICATION_JSON).content(userJson))
				.andDo(print())
				.andExpect(header().string("Location",is(equalTo("http://localhost/users/4"))))
				.andExpect(status().isCreated());

		mockMvc.perform(get("/users/4"))
				.andDo(print())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.firstname", is(equalTo("Saint"))))
				.andExpect(jsonPath("$.lastname", is(equalTo("Nicholas"))))
				.andExpect(jsonPath("$.username", is(equalTo("santa"))))
				.andExpect(status().isOk());
	}

	@Test
	public void testCreateUserTask() throws Exception {
		String userTaskJson = "{\"description\":\"desc\",\"completed\":false,\"dueDate\":\"2015-12-31T23:00:00.000+0000\",\"priority\":\"HIGH\"}";
		mockMvc.perform(
				post("/users/1/tasks").contentType(MediaType.APPLICATION_JSON).content(userTaskJson))
				.andDo(print())
				.andExpect(header().string("Location",is(equalTo("http://localhost/users/1/tasks/4"))))
				.andExpect(status().isCreated());

		mockMvc.perform(get("/users/1/tasks/4"))
				.andDo(print())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.description", is(equalTo("desc"))))
				.andExpect(jsonPath("$.completed", is(equalTo(false))))
				.andExpect(jsonPath("$.dueDate",is(equalTo("2015-12-31T23:00:00.000+0000"))))
				.andExpect(jsonPath("$.priority", is(equalTo("HIGH"))))
				.andExpect(status().isOk());

	}

	@Test
	public void testGetUser2Task1() throws Exception {
		mockMvc.perform(get("/users/2/tasks/1"))
				.andDo(print())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.description",is(equalTo("task twenty one"))))
				.andExpect(jsonPath("$.completed", is(equalTo(false))))
				.andExpect(jsonPath("$.dueDate",is(equalTo("2016-01-04T04:05:06.000+0000"))))
				.andExpect(jsonPath("$.priority",is(equalTo("HIGH"))))
				.andExpect(status().isOk());
	}

}
