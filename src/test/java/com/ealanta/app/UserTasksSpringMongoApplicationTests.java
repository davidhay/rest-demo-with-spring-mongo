package com.ealanta.app;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.ealanta.app.domain.Priority;
import com.ealanta.app.domain.Task;
import com.ealanta.app.domain.User;
import com.ealanta.app.repo.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserTasksSpringMongoApplication.class)
@WebAppConfiguration
public class UserTasksSpringMongoApplicationTests {

	@Autowired
	private UserRepository userRepo;

	@Test
	public void contextLoads() {
		//Assert.assertEquals(1,userRepo.count());
		List<User> users = userRepo.findAll();
		for (User user : users) {
			System.out.println(user);
		}
	}
	
	@Test
	public void testInsert() {
		userRepo.deleteAll();
		User user = new User();
		user.setId(1);
		user.setUsername("spiderman");
		user.setFirst("Peter");
		user.setLast("Parker");
		
		Task task1 = new Task();
		task1.setCompleted(false);
		task1.setDueDate(new Date());
		task1.setPriority(Priority.LO);
		task1.setDescription("task 1");
		
		Task task2 = new Task();
		task2.setCompleted(false);
		task2.setDueDate(new Date());
		task2.setPriority(Priority.MED);
		task2.setDescription("task 2");

		Task task3 = new Task();
		task3.setCompleted(false);
		task3.setDueDate(new Date());
		task3.setPriority(Priority.HIGH);
		task3.setDescription("task 3");
		
		user.setTasks(Arrays.asList(task1,task2,task3));
		userRepo.save(user);
	}

}
